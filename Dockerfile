FROM python:3.6-buster

COPY requirements.txt /tmp/requirements.txt

RUN pip install -r /tmp/requirements.txt && \
    useradd -u 1612 audit && \
    mkdir /app && \
    chown audit:audit /app

USER audit

WORKDIR /app

ENV PYTHONPATH=/app

COPY . .
